package com.example.guessme2;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class Setting extends Activity implements OnClickListener {
	int state,score;
	ArrayList<Integer> stateLevel;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		//stateLevel= new ArrayList<Integer>();
		
		Intent i = this.getIntent();
	        if(i.hasExtra("state"))
			{
	        
	        	state = i.getIntExtra("state",-1);
	        	score = i.getIntExtra("score",-1);
	        	stateLevel = i.getIntegerArrayListExtra("stateLevel");
			}
		
		ImageButton s1 = (ImageButton)findViewById(R.id.resetBT);
		ImageButton s2 = (ImageButton)findViewById(R.id.backBT);
		Button s3 = (Button)findViewById(R.id.okBT);
		
		s1.setOnClickListener(this);
		s2.setOnClickListener(this);
		s3.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.setting, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		
		switch(id){
		case R.id.resetBT: // go to level selection
			Intent i = new Intent(getApplicationContext(),State.class);
			startActivity(i);
			break;
		
		case R.id.backBT: // go to menu
			Intent i2 = new Intent(getApplicationContext(),MenuSelection.class);
			i2.putExtra("score", score);
			i2.putIntegerArrayListExtra("stateLevel", stateLevel);
			
			this.setResult(RESULT_OK, i2);

			this.finish();
			startActivity(i2);
			break;
			
		case R.id.okBT: // back to current level
			Intent i3 = new Intent(getApplicationContext(),MainActivity.class);
			i3.putExtra("state", state);
			i3.putExtra("score", score);
			i3.putIntegerArrayListExtra("stateLevel", stateLevel);
			this.setResult(RESULT_OK, i3);
			this.finish();
			startActivity(i3);
			// still not go back to current level, pls fix
			break;
			
		}
	}

}
