/*
 * ITS333 Term Project
 * By
 * 5422780858 Pichaya Prasertsung
 * 5422781658 Peangthan Pinitwararak
 * 5422791376 Techinee Suwimol
 * 
 * 
 * 
 * 
 * 
 * */

package com.example.guessme2;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class Congrat extends Activity implements OnClickListener {
	
	
	ArrayList<Integer> stateLevel;
	int flag =0;
	int score;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_congrat);
		
		ImageButton bt = (ImageButton)findViewById(R.id.backFromCon);
		bt.setOnClickListener(this);
		Intent i = this.getIntent();
		score = i.getIntExtra("score",0);
		 if(i.hasExtra("stateLevel"))
	        {
	        	stateLevel = i.getIntegerArrayListExtra("stateLevel");
	        	flag = 1;
	        }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.congrat, menu);
		return true;
	}

	public void onBackPressed() {
			
			
	}
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		Intent i2 = new Intent(getApplicationContext(),MenuSelection.class);
		i2.putExtra("score", score);
		if(flag==1){
			
			i2.putIntegerArrayListExtra("stateLevel", stateLevel);
			
		}
		
		startActivity(i2);
		
	}
}