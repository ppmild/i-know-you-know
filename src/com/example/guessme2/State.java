package com.example.guessme2;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class State extends Activity
implements OnClickListener {
	int state;
	int score;
	int flag=0;
	int[] chk = new int[20];
	List<Map<String,String>> list;
	SimpleAdapter adapter;
	ArrayList<Integer> stateLevel;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_state);
		
		
		 Intent i = this.getIntent();
		 if(i.hasExtra("stateLevel"))
	        {
	        	stateLevel = i.getIntegerArrayListExtra("stateLevel");
	        	
	        	flag = 1;
	        	for(int j=0;j<20;j++)
				{
					chk[j] = stateLevel.get(j);
					
				}
	        }
	        if(i.hasExtra("score"))
			{
	        	score = i.getIntExtra("score",-1);
	        	
	        	
			}
		
	    TextView lv = (TextView)findViewById(R.id.currentScore);
	    lv.setText("score " + score);
	        
		ImageButton l01 = (ImageButton)findViewById(R.id.l01);
		l01.setOnClickListener(this);
	
		ImageButton l02 = (ImageButton)findViewById(R.id.l02);
		
        l02.setOnClickListener(this);
        
        ImageButton l03 = (ImageButton)findViewById(R.id.l03);
        l03.setOnClickListener(this);
        
        ImageButton l04 = (ImageButton)findViewById(R.id.l04);
        l04.setOnClickListener(this);
        
        ImageButton l05 = (ImageButton)findViewById(R.id.l05);
        l05.setOnClickListener(this);
        
        ImageButton l06 = (ImageButton)findViewById(R.id.l06);
        l06.setOnClickListener(this);
        
        ImageButton l07 = (ImageButton)findViewById(R.id.l07);
        l07.setOnClickListener(this);
        
        ImageButton l08 = (ImageButton)findViewById(R.id.l08);
        l08.setOnClickListener(this);
        
        ImageButton l09 = (ImageButton)findViewById(R.id.l09);
        l09.setOnClickListener(this);
        
        ImageButton l10 = (ImageButton)findViewById(R.id.l10);
        l10.setOnClickListener(this);
        
        ImageButton l11 = (ImageButton)findViewById(R.id.l11);
        l11.setOnClickListener(this);
        
        ImageButton l12 = (ImageButton)findViewById(R.id.l12);
        l12.setOnClickListener(this);
        
        ImageButton l13 = (ImageButton)findViewById(R.id.l13);
        l13.setOnClickListener(this);
        
        ImageButton l14 = (ImageButton)findViewById(R.id.l14);
        l14.setOnClickListener(this);
        
        ImageButton l15 = (ImageButton)findViewById(R.id.l15);
        l15.setOnClickListener(this);
        
        ImageButton l16 = (ImageButton)findViewById(R.id.l16);
        l16.setOnClickListener(this);
        
        ImageButton l17 = (ImageButton)findViewById(R.id.l17);
        l17.setOnClickListener(this);
        
        ImageButton l18 = (ImageButton)findViewById(R.id.l18);
        l18.setOnClickListener(this);
        
        ImageButton l19 = (ImageButton)findViewById(R.id.l19);
        l19.setOnClickListener(this);
        
        ImageButton l20 = (ImageButton)findViewById(R.id.l20);
        l20.setOnClickListener(this);
	
        if(chk[0]==1)
			l01.setBackgroundColor(Color.GREEN);
        if(chk[1]==1)
        	l02.setBackgroundColor(Color.GREEN);
        if(chk[2]==1)
        	l03.setBackgroundColor(Color.GREEN); 
        if(chk[3]==1)
            l04.setBackgroundColor(Color.GREEN); 
        if(chk[4]==1)
            l05.setBackgroundColor(Color.GREEN);
        if(chk[5]==1)
            l06.setBackgroundColor(Color.GREEN);
        if(chk[6]==1)
            l07.setBackgroundColor(Color.GREEN);
        if(chk[7]==1)
            l08.setBackgroundColor(Color.GREEN);
        if(chk[8]==1)
            l09.setBackgroundColor(Color.GREEN);
        if(chk[9]==1)
            l10.setBackgroundColor(Color.GREEN);
        if(chk[10]==1)
            l11.setBackgroundColor(Color.GREEN);
        if(chk[11]==1)
            l12.setBackgroundColor(Color.GREEN);
        if(chk[12]==1)
            l13.setBackgroundColor(Color.GREEN);
        if(chk[13]==1)
            l14.setBackgroundColor(Color.GREEN);
        if(chk[14]==1)
            l15.setBackgroundColor(Color.GREEN);
        if(chk[15]==1)
            l16.setBackgroundColor(Color.GREEN);
        if(chk[17]==1)
            l18.setBackgroundColor(Color.GREEN);
        if(chk[18]==1)
            l19.setBackgroundColor(Color.GREEN);
        if(chk[19]==1)
            l20.setBackgroundColor(Color.GREEN);
      
	}

	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.state, menu);
		return true;
	}

	public void onClick(View v) {
		
		Intent i = new Intent(getApplicationContext(), MainActivity.class);
		i.putExtra("score", score);
		if(flag==1){
			i.putIntegerArrayListExtra("stateLevel", stateLevel);
			
		}
		switch(v.getId())
	    {
	    case R.id.l01 :
	    	
	        //Toast.makeText(FullscreenActivity.this,"button1", 1000).show();
	    	state = 1;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t = Toast.makeText(this, "start level1", 
					Toast.LENGTH_SHORT);
			t.show();
	        startActivity(i);
	        
	        break;
	        
	    case R.id.l02 :
	    	state = 2;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t2 = Toast.makeText(this, "start level2", 
					Toast.LENGTH_SHORT);
			t2.show();
	        startActivity(i);
	        break;
	        
	    case R.id.l03 :
	    	state = 3;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t3 = Toast.makeText(this, "start level3", 
					Toast.LENGTH_SHORT);
			t3.show();
	        startActivity(i);
	     
	        break;  
	        
	    case R.id.l04 :
	    	state = 4;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t4 = Toast.makeText(this, "start level4", 
					Toast.LENGTH_SHORT);
			t4.show();
	        startActivity(i);
	     
	        break;  
	        
	    case R.id.l05 :
	    	state = 5;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t5 = Toast.makeText(this, "start level5", 
					Toast.LENGTH_SHORT);
			t5.show();
	        startActivity(i);
	     
	        break;  
	    
	    case R.id.l06 :
	    	state = 6;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t6 = Toast.makeText(this, "start level6", 
					Toast.LENGTH_SHORT);
			t6.show();
	        startActivity(i);
	     
	        break;  
	    
	    case R.id.l07 :
	    	state = 7;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t7 = Toast.makeText(this, "start level 7", 
					Toast.LENGTH_SHORT);
			t7.show();
	        startActivity(i);
	     
	        break; 
	        
	    case R.id.l08 :
	    	state = 8;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t8 = Toast.makeText(this, "start level8", 
					Toast.LENGTH_SHORT);
			t8.show();
	        startActivity(i);
	     
	        break; 
	        
	    case R.id.l09 :
	    	state = 9;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t9 = Toast.makeText(this, "start level 9", 
					Toast.LENGTH_SHORT);
			t9.show();
	        startActivity(i);
	     
	        break; 
	        
	    case R.id.l10 :
	    	state = 10;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t10 = Toast.makeText(this, "start level 10", 
					Toast.LENGTH_SHORT);
			t10.show();
	        startActivity(i);
	        break; 
	        
	    case R.id.l11 :
	    	state = 11;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t11 = Toast.makeText(this, "start level 11", 
					Toast.LENGTH_SHORT);
			t11.show();
	        startActivity(i);
	        break; 
	        
	    case R.id.l12 :
	    	state = 12;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t12 = Toast.makeText(this, "start level 12", 
					Toast.LENGTH_SHORT);
			t12.show();
	        startActivity(i);
	        break; 

	    case R.id.l13 :
	    	state = 13;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t13 = Toast.makeText(this, "start level 13", 
					Toast.LENGTH_SHORT);
			t13.show();
	        startActivity(i);
	        break; 
	        
	    case R.id.l14 :
	    	state = 14;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t14 = Toast.makeText(this, "start level 14", 
					Toast.LENGTH_SHORT);
			t14.show();
	        startActivity(i);
	        break; 
	        
	        
	    case R.id.l15 :
	    	state = 15;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t15 = Toast.makeText(this, "start level 15", 
					Toast.LENGTH_SHORT);
			t15.show();
	        startActivity(i);
	        break; 
	        
	        
	    case R.id.l16 :
	    	state = 16;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t16 = Toast.makeText(this, "start level 16", 
					Toast.LENGTH_SHORT);
			t16.show();
	        startActivity(i);
	        break; 
	        
	    case R.id.l17 :
	    	state = 17;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t17 = Toast.makeText(this, "start level 17", 
					Toast.LENGTH_SHORT);
			t17.show();
	        startActivity(i);
	        break; 
	        
	    case R.id.l18 :
	    	state = 18;
			i.putExtra("state", state);
			this.setResult(RESULT_OK, i);
			this.finish();
			Toast t18 = Toast.makeText(this, "start level 18", 
					Toast.LENGTH_SHORT);
			t18.show();
	        startActivity(i);
	        break; 
	    
	    case R.id.l19 :
		    	state = 19;
				i.putExtra("state", state);
				this.setResult(RESULT_OK, i);
				this.finish();
				Toast t19 = Toast.makeText(this, "start level 19", 
						Toast.LENGTH_SHORT);
				t19.show();
		        startActivity(i);
		        break; 
		        
	    
	    case R.id.l20 :
			    	state = 20;
					i.putExtra("state", state);
					this.setResult(RESULT_OK, i);
					this.finish();
					Toast t20 = Toast.makeText(this, "start level 20", 
							Toast.LENGTH_SHORT);
					t20.show();
			        startActivity(i);
			        break; 
	    
	    }
		
		
	
	}
	

	
}
