package com.example.guessme2;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

public class MenuSelection extends Activity implements OnClickListener {
	
	int score;
	int flag=0;
	ArrayList<Integer> stateLevel;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu_selection);
		
		ImageButton Bplace = (ImageButton)findViewById(R.id.m_place);
        Bplace.setOnClickListener(this);
        
        ImageButton Bfruit = (ImageButton)findViewById(R.id.m_fruit);
        Bfruit.setOnClickListener(this);
        
        Intent i = this.getIntent();
        if(i.hasExtra("stateLevel"))
        {	flag = 1;
        	stateLevel = i.getIntegerArrayListExtra("stateLevel");
        	
        }
        if(i.hasExtra("score"))
		{
               	score = i.getIntExtra("score",-1);
        	
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_selection, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
	    {
	    case R.id.m_place :
	        //Toast.makeText(FullscreenActivity.this,"button1", 1000).show();
	        Intent i = new Intent(getApplicationContext(), State.class);
			i.putExtra("score", score);
			if(flag==1){
				i.putIntegerArrayListExtra("stateLevel", stateLevel);
			}
				this.setResult(RESULT_OK, i);
			this.finish();
			
	        startActivity(i);

	        break;
	    case R.id.m_fruit :
	        Toast.makeText(MenuSelection.this,"Coming Soon", 1000).show();
	        break;
	  


	    }
	}

}
